use crate::{execute, OperationOutput};
use js_sys::{Map, Uint8Array};
#[cfg(debug_assertions)]
use std::panic;
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::JsValue;

/// Fetch a file from the given URL and produce its bytes as output.
/// This is the WASM binding of the Operation `execute()` function
///
/// # Arguments
///  * operation_inputs: collection of serialized inputs
///  * config: configuration map
///
/// # Return
/// A promise that resolves to [`OperationOutput`] if it succeeds, or to an
/// exception otherwise
#[wasm_bindgen(js_name=execute)]
pub async fn wasm_execute(
    operation_inputs: Vec<Uint8Array>,
    config: Map,
) -> Result<OperationOutput, JsValue> {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    let deserialized: String = JsValue::from(operation_inputs.get(0).unwrap())
        .as_string()
        .unwrap();
    let config_map = config.into_serde().unwrap();

    execute(&deserialized, config_map)
        .await
        .map(|operation_output| OperationOutput::downcast(operation_output))
        .map_err(|error| JsValue::from(error.to_string()))
}

/// Output manifest data
#[wasm_bindgen(js_name=describe)]
pub fn wasm_describe() -> String {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    crate::describe()
}
