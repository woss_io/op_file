use an_operation_support::{operation_return_type, Bytes};
#[cfg(feature = "wasm")]
use wasm_bindgen::prelude::*;

/// The Operation output. Declares return type [`Bytes`].
/// Has WASM binding. Is available as dependency for other Operations
#[cfg(feature = "anagolay")]
#[cfg_attr(feature = "wasm", wasm_bindgen)]
#[derive(Debug, Clone)]
#[operation_return_type(Bytes)]
pub struct OperationOutput {
    /// The buffer returned as output
    data: Bytes,
}

#[cfg(feature = "anagolay")]
#[cfg_attr(feature = "wasm", wasm_bindgen)]
impl OperationOutput {
    /// Transform this output into an input for another Operation
    ///
    /// # Return
    /// A javascript array of 8-bit unsigned integers containing the bytes of
    /// the fetched file
    #[cfg(target_arch = "wasm32")]
    pub fn as_input(&self) -> js_sys::Uint8Array {
        an_operation_support::operation::OperationInput::from_bytes(&self.data)
    }

    /// Transform this output into an input for another Operation
    ///
    /// # Return
    /// The bytes of the fetched file
    #[cfg(not(target_arch = "wasm32"))]
    pub fn as_input(&self) -> Bytes {
        self.data.clone()
    }

    /// Constructor
    ///
    /// # Arguments
    ///  * data: the bytes of the fetched file
    ///
    /// # Return
    /// The instance
    #[cfg_attr(feature = "wasm", wasm_bindgen(constructor))]
    pub fn new(data: Bytes) -> OperationOutput {
        OperationOutput { data }
    }

    /// # Return
    /// A copy of the bytes of the fetched file.
    /// No decoding involved in this case since the bytes were not stored encoded
    pub fn decode(&self) -> Bytes {
        self.data.clone()
    }
}
