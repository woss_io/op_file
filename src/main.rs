use op_file::describe;

/// Main function. Uses shared main implementation for common task
pub fn main() {
    let main_args = an_operation_support::MainArgs {
        app_name: env!("CARGO_PKG_NAME"),
        app_version: env!("CARGO_PKG_VERSION"),
        describe: &|| crate::describe(),
    };
    an_operation_support::main(&main_args);
}
